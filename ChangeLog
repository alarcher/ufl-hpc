1.0.1 [2013-01-07]
 - Some memory usage optimizations.
1.0.0 [2011-12-07]
 - No changes since rc1.
1.0-rc1 [2011-11-22]
 - Added tests covering snippets from UFL chapter in FEniCS book
 - Added more unit tests
 - Added operators diag and diag_vector
 - Added geometric quantities cell.surface_area and cell.facet_area
 - Fixed rtruediv bug
 - Fixed bug with derivatives of elements of type Real with unspecified cell
1.0-beta3 [2011-10-26]
 - Added nabla_grad and nabla_div operators
 - Added error function erf(x)
 - Added bessel functions of first and second kind, normal and modified,
   bessel_J(nu, x), bessel_Y(nu, x), bessel_I(nu, x), bessel_K(nu, x)
 - Extended derivative() to allow indexed coefficient(s) as differentiation variable
 - Made *Constant use the 'Real' space instead of 'DG0'
 - Bugfix in adjoint where test and trial functions were in different spaces
 - Bugfix in replace where the argument to a grad was replaced with 0
 - Bugfix in reconstruction of tensor elements
 - Some other minor bugfixes
1.0-beta2 [2011-08-11]
 - Support c*form where c depends on a coefficient in a Real space
1.0-beta [2011-07-08]
 - Add script ufl-version
 - Added syntax for associating an arbitrary domain data object with a measure:
	dss = ds[boundaries]; M = f*dss(1) + g*dss(2)
 - Added new operators elem_mult, elem_div, elem_pow and elem_op for
   elementwise application of scalar operators to tensors of equal shape
 - Added condition operators And(lhs,rhs) and Or(lhs,rhs) and Not(cond)
 - Fixed support for symmetries in subelements of a mixed element
 - Add support for specifying derivatives of coefficients to derivative()
0.9.1 [2011-05-16]
 - Remove set_foo functions in finite element classes
 - Change license from GPL v3 or later to LGPL v3 or later
 - Change behavior of preprocess(), form.compute_form_data(), form_data.preprocessed_form
 - Allowing grad, div, inner, dot, det, inverse on scalars
 - Simplify Identity(1) -> IntValue(1) automatically
 - Added Levi-Cevita symbol: e = PermutationSymbol(3); e[i,j,k]
 - Fix bug with future division behaviour (ufl does not support floor division)
 - Add subdomain member variables to form class
 - Allow action on forms of arbitrary rank
0.9.0 [2011-02-23]
 - Allow jump(Sigma, n) for matrix-valued expression Sigma
 - Bug fix in scalar curl operator
 - Bug fix in deviatoric operator
0.5.4 [2010-09-01]
 - Bug fixes in PartExtracter
 - Do not import x for coordinate
 - Add Circumradius to Cell (Cell.circumradius)
 - Add CellVolume to Cell (Cell.volume)
0.5.3 [2010-07-01]
 - Rename ElementRestriction --> RestrictedElement
 - Experimental import of x from tetrahedron
 - Make lhs/rhs work for resrictions
 - Redefine operator + for FiniteElements and replace + by *
 - Rename ElementUnion -> EnrichedElement
 - Add support for tan() and inverse trigonometric functions
0.5.2 [2010-02-15]
 - Attach form data to preprocessed form, accessible by form.form_data()
0.5.1 [2010-02-03]
 - Fix bug in propagate_restriction
0.5.0 [2010-02-01]
 - Several interface changes in FormData class
 - Introduce call preprocess(form) to be called at beginning of compilation
 - Rename BasisFunction --> Argument
 - Rename Function --> Coefficient
0.4.1 [2009-12-04]
 - Redefine grad().T --> grad()
 - New meaning of estimate_max_polynomial_degree
 - New function estimate_total_polynomial_degree
 - Allow degree = None and cell = None for elements
0.4.0 [2009-09-23]
 - Extensions for ElementRestriction (restrict FiniteElement to Cell)
 - Bug fix for lhs/rhs with list tensor types
 - Add new log function set_prefix
 - Add new log function log(level, message)
 - Added macro cell integral *dE
 - Added mechanism to add additional integral types
 - Added LiftingOperator and LiftingFunction
 - Added ElementRestriction
0.3.0 [2009-05-28]
 - Some critical bugfixes, in particular in differentiation.
 - Added form operators "system" and "sensitivity_rhs".
 - diff can take form as argument, applies to all integrands.
 - Rudimentary precedence handling for better
   use of parentheses in str(expression).
 - Added script ufl2py, mainly for debugging purposes.
 - Crude implementation of estimate_max_polynomial_degree
   for quadrature degree estimation.
 - Improved manual.
0.2.0 [2009-04-07]
 - Initial release of UFL.
0.1.0 [...]
 - Unreleased development versions of UFL.
